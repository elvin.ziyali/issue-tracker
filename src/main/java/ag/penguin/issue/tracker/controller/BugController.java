package ag.penguin.issue.tracker.controller;

import ag.penguin.issue.tracker.dto.BugDto;
import ag.penguin.issue.tracker.service.BugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/issue/bug")
@Api(value = "Operations related to Bugs")
public class BugController {

    private final BugService service;

    @GetMapping()
    @ApiOperation(value = "Get all saved bugs")
    public ResponseEntity<List<BugDto>> getAllBug() {
        var bugs = service.getAllBug();
        return ResponseEntity.ok(bugs);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get specified bug with id")
    public ResponseEntity<BugDto> getBug(@PathVariable @NotBlank String id) {
        var bug = service.getBug(id);
        return ResponseEntity.ok(bug);
    }

    @PostMapping()
    @ApiOperation(value = "Create new bug")
    public ResponseEntity<String> createBug(@RequestBody @Valid BugDto bugDto) {
        var save = service.save(bugDto);
        return ResponseEntity.ok(save);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update bug with given id")
    public ResponseEntity<String> updateBug(@PathVariable @NotBlank String id,
                                            @RequestBody @NotNull BugDto bugDto) {
        var update = service.update(id, bugDto);
        return ResponseEntity.ok(update);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete bug with given id")
    public ResponseEntity<String> deleteBug(@PathVariable @NotBlank String id) {
        var delete = service.deleteBug(id);
        return ResponseEntity.ok(delete);
    }
}
