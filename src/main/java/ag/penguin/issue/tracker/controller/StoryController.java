package ag.penguin.issue.tracker.controller;

import ag.penguin.issue.tracker.dto.StoryDto;
import ag.penguin.issue.tracker.service.StoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("/issue/story")
@Api(value = "Operations related to Stories")
public class StoryController {

    private final StoryService service;

    @GetMapping()
    @ApiOperation(value = "Get all saved stories")
    public ResponseEntity<List<StoryDto>> getAllStories() {
        var stories = service.getAllStories();
        return ResponseEntity.ok(stories);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get specified story with id")
    public ResponseEntity<StoryDto> getStory(@PathVariable @NotBlank String id) {
        var story = service.getStory(id);
        return ResponseEntity.ok(story);
    }

    @PostMapping()
    @ApiOperation(value = "Create new story")
    public ResponseEntity<String> createStory(@RequestBody @NotNull StoryDto storyDto) {
        var save = service.save(storyDto);
        return ResponseEntity.ok(save);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update story with given id")
    public ResponseEntity<String> updateStory(
            @PathVariable @NotBlank String id,
            @RequestBody @NotNull StoryDto storyDto) {
        var update = service.update(id, storyDto);
        return ResponseEntity.ok(update);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete story with given id")
    public ResponseEntity<String> deleteStory(@PathVariable @NotNull String id) {
        var delete = service.deleteStory(id);
        return ResponseEntity.ok(delete);
    }

}
