package ag.penguin.issue.tracker.controller;

import ag.penguin.issue.tracker.dto.AssignmentDto;
import ag.penguin.issue.tracker.service.WeekPlanner;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class WeekPlannerController {
    private final WeekPlanner weekPlanner;

    @GetMapping()
    @ApiOperation(value = "Get weekly plan for number of developers")
    public ResponseEntity<List<AssignmentDto>> getWeeklyPlan() {
        var assignment = weekPlanner.getAssignment();
        return ResponseEntity.ok(assignment);
    }
}
