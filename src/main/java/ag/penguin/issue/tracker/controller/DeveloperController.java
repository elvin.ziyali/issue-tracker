package ag.penguin.issue.tracker.controller;

import ag.penguin.issue.tracker.dto.DeveloperDto;
import ag.penguin.issue.tracker.service.DeveloperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/developer")
@Api(value = "Operations related to Developers")
public class DeveloperController {

    private final DeveloperService service;

    @GetMapping()
    @ApiOperation(value = "Get all saved developers")
    public ResponseEntity<List<DeveloperDto>> getAllDevelopers() {
        var allDevelopers = service.getAllDevelopers();
        return ResponseEntity.ok(allDevelopers);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get specified developers with id")
    public ResponseEntity<DeveloperDto> getDeveloper(@PathVariable @NotNull String id) {
        var developer = service.getDeveloper(id);
        return ResponseEntity.ok(developer);
    }

    @PostMapping()
    @ApiOperation(value = "Create/save new developer")
    public ResponseEntity<String> createDeveloper(@RequestBody @Valid @NotNull DeveloperDto developerDto) {
        var save = service.save(developerDto);
        return ResponseEntity.ok(save);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update developer with given id")
    public ResponseEntity<String> updateDeveloper(@PathVariable @NotBlank String id,
                                                  @RequestBody @Valid DeveloperDto developerDto) {
        var update = service.update(id, developerDto);
        return ResponseEntity.ok(update);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete developer with given id")
    public ResponseEntity<String> deleteDeveloper(@PathVariable @NotBlank String id) {
        String delete = service.deleteDeveloper(id);
        return ResponseEntity.ok(delete);
    }

}
