package ag.penguin.issue.tracker.service;

import ag.penguin.issue.tracker.dto.AssignmentDto;

import java.util.List;

public interface WeekPlanner {
    List<AssignmentDto> getAssignment();
}
