package ag.penguin.issue.tracker.service;


import ag.penguin.issue.tracker.dto.BugDto;

import java.util.List;

public interface BugService {
    List<BugDto> getAllBug();

    BugDto getBug(String id);

    String save(BugDto bugDto);

    String update(String id, BugDto bugDto);

    String deleteBug(String id);
}
