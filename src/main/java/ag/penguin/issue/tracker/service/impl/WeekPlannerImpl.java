package ag.penguin.issue.tracker.service.impl;

import ag.penguin.issue.tracker.dto.AssignmentDto;
import ag.penguin.issue.tracker.dto.StoryDto;
import ag.penguin.issue.tracker.service.DeveloperService;
import ag.penguin.issue.tracker.service.StoryService;
import ag.penguin.issue.tracker.service.WeekPlanner;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WeekPlannerImpl implements WeekPlanner {
    private final StoryService storyService;
    private final DeveloperService developerService;

    @Override
    public List<AssignmentDto> getAssignment() {
        int MAX_POINT = 10;
        long count = developerService.countDeveloper();
        var assignmentDtos = new ArrayList<AssignmentDto>();
        var stories = storyService.getAllStoryPointSorted();
        for (int i = 0; i < count; i++) {

            ArrayList<StoryDto> task = new ArrayList<>();
            int point = 0;
            for (int j = stories.size() - 1; j >= 0 && point <= MAX_POINT; j--) {
                if (stories.get(j).getPoint() + point <= MAX_POINT) {
                    point += stories.get(j).getPoint();
                    task.add(stories.get(j));
                    stories.remove(j);
                }
            }
            AssignmentDto assignment = AssignmentDto.builder()
                    .index(i).assignedPoint(point).assignedTasks(task)
                    .build();
            assignmentDtos.add(assignment);
        }

        return assignmentDtos;
    }
}
