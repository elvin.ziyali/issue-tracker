package ag.penguin.issue.tracker.service.impl;

import ag.penguin.issue.tracker.dto.StoryDto;
import ag.penguin.issue.tracker.entity.Developer;
import ag.penguin.issue.tracker.entity.Story;
import ag.penguin.issue.tracker.exception.DataNotFoundException;
import ag.penguin.issue.tracker.mapper.StoryMapper;
import ag.penguin.issue.tracker.repository.DeveloperRepository;
import ag.penguin.issue.tracker.repository.StoryRepository;
import ag.penguin.issue.tracker.service.StoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StoryServiceImpl implements StoryService {

    private final StoryRepository repository;
    private final DeveloperRepository developerRepository;
    private final StoryMapper mapper;

    @Override
    public List<StoryDto> getAllStories() {
        var allStories = repository.findAll();
        if (allStories.isEmpty()) {
            throw new DataNotFoundException("No Story found");
        }
        return mapper.mapToDtoList(allStories);
    }

    @Override
    public StoryDto getStory(String id) {
        var story = repository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("No story found with given id"));
        return mapper.mapToDto(story);
    }

    @Override
    public String save(StoryDto storyDto) {
        Story story = mapper.mapToEntity(storyDto);
        var developerById = this.getDeveloperOrNull(storyDto.getDeveloperId());
        story.setDeveloper(developerById);
        repository.save(story);
        return "Story saved";
    }

    @Override
    public String update(String id, StoryDto storyDto) {
        this.getStory(id);
        Story story = mapper.mapToEntity(storyDto);
        var developerById = this.getDeveloperOrNull(storyDto.getDeveloperId());
        story.setId(id);
        story.setDeveloper(developerById);
        repository.save(story);
        return "Story updated";
    }

    @Override
    public String deleteStory(String id) {
        this.getStory(id);
        repository.deleteById(id);
        return "Story deleted";
    }

    public List<StoryDto> getAllStoryPointSorted() {
        List<Story> allByOrderByPointAsc = repository.findAllByOrderByPointAsc();
        return mapper.mapToDtoList(allByOrderByPointAsc);
    }

    private Developer getDeveloperOrNull(String id) {
        return id == null ? null : developerRepository.findById(id).orElse(null);
    }
}
