package ag.penguin.issue.tracker.service.impl;

import ag.penguin.issue.tracker.dto.DeveloperDto;
import ag.penguin.issue.tracker.entity.Developer;
import ag.penguin.issue.tracker.exception.DataNotFoundException;
import ag.penguin.issue.tracker.mapper.DeveloperMapper;
import ag.penguin.issue.tracker.repository.DeveloperRepository;
import ag.penguin.issue.tracker.service.DeveloperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeveloperServiceImpl implements DeveloperService {
    private final DeveloperRepository repository;
    private final DeveloperMapper mapper;

    @Override
    public List<DeveloperDto> getAllDevelopers() {
        List<Developer> allDevelopers = repository.findAll();
        if (allDevelopers.isEmpty()) {
            throw new DataNotFoundException("No Developer Found");
        }
        List<DeveloperDto> developers = mapper.mapToDtoList(allDevelopers);
        return developers;
    }

    @Override
    public DeveloperDto getDeveloper(String id) {
        Developer developer = repository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("No developer found with given id"));
        return mapper.mapToDto(developer);
    }

    @Override
    public String save(DeveloperDto developerDto) {
        Developer developer = mapper.mapToEntity(developerDto);
        repository.save(developer);
        return "Developer saved";
    }

    @Override
    public String update(String id, DeveloperDto developerDto) {
        this.getDeveloper(id);
        Developer developer = mapper.mapToEntity(developerDto);
        developer.setId(id);
        repository.save(developer);
        return "Developer updated";
    }

    @Override
    public String deleteDeveloper(String id) {
        this.getDeveloper(id);
        repository.deleteById(id);
        return "Developer deleted from DB";
    }

    @Override
    public long countDeveloper() {
        return repository.count();
    }
}
