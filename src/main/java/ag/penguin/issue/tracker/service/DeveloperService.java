package ag.penguin.issue.tracker.service;

import ag.penguin.issue.tracker.dto.DeveloperDto;

import java.util.List;

public interface DeveloperService {

    List<DeveloperDto> getAllDevelopers();

    DeveloperDto getDeveloper(String id);

    String save(DeveloperDto developerDto);

    String update(String id, DeveloperDto developerDto);

    String deleteDeveloper(String id);

    long countDeveloper();

}

