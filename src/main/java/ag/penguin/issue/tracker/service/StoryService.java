package ag.penguin.issue.tracker.service;

import ag.penguin.issue.tracker.dto.StoryDto;

import java.util.List;

public interface StoryService {
    List<StoryDto> getAllStories();

    StoryDto getStory(String id);

    String save(StoryDto storyDto);

    String update(String id, StoryDto storyDto);

    String deleteStory(String id);

    List<StoryDto> getAllStoryPointSorted();
}
