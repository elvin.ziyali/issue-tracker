package ag.penguin.issue.tracker.service.impl;

import ag.penguin.issue.tracker.dto.BugDto;
import ag.penguin.issue.tracker.entity.Bug;
import ag.penguin.issue.tracker.entity.Developer;
import ag.penguin.issue.tracker.exception.DataNotFoundException;
import ag.penguin.issue.tracker.mapper.BugMapper;
import ag.penguin.issue.tracker.repository.BugRepository;
import ag.penguin.issue.tracker.repository.DeveloperRepository;
import ag.penguin.issue.tracker.service.BugService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BugServiceImpl implements BugService {
    private final DeveloperRepository developerRepository;
    private final BugRepository repository;
    private final BugMapper mapper;

    @Override
    public List<BugDto> getAllBug() {
        List<Bug> all = repository.findAll();
        if (all.isEmpty()) {
            throw new DataNotFoundException("No Bug found");
        }
        return mapper.mapToDtoList(all);
    }

    @Override
    public BugDto getBug(String id) {
        Bug bug = repository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("No bug found with given id"));
        return mapper.mapToDto(bug);
    }

    @Override
    public String save(BugDto bugDto) {
        Bug bug = mapper.mapToEntity(bugDto);
        bug.setDeveloper(this.getDeveloperOrNull(bugDto.getDeveloperId()));
        repository.save(bug);
        return "Bug saved";
    }

    @Override
    public String update(String id, BugDto bugDto) {
        this.getBug(id);
        Bug bug = mapper.mapToEntity(bugDto);
        bug.setDeveloper(this.getDeveloperOrNull(bugDto.getDeveloperId()));
        bug.setId(id);
        repository.save(bug);
        return "Bug updated";
    }

    @Override
    public String deleteBug(String id) {
        this.getBug(id);
        repository.deleteById(id);
        return "Bug saved";
    }

    private Developer getDeveloperOrNull(String id) {
        return id == null ? null : developerRepository.findById(id).orElse(null);
    }
}
