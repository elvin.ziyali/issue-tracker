package ag.penguin.issue.tracker.enums;

public enum Status {
    NEW("New"),
    ESTIMATED("Estimated"),
    COMPLETED("Completed");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
