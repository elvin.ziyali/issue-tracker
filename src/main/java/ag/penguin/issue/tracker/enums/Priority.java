package ag.penguin.issue.tracker.enums;


public enum Priority {
    CRITICAL("Critical"),
    MAJOR("Major"),
    MINOR("Minor");

    private final String value;

    Priority(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
