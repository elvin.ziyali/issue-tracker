package ag.penguin.issue.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IssueTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IssueTrackerApplication.class, args);
    }

    public static void find(int[] A, int currSum, int index, int sum, int[] solution) {
        if (currSum == sum) {

            System.out.print("Output: [");
            for (int i = 0; i < solution.length; i++) {
                if (solution[i] == 1) {
                    if (A[i] != 0) {
                        System.out.print("  " + A[i]);
                    }
                }
            }
            System.out.print(" ]\n");

        } else if (index == A.length) {
            return;
        } else {
            solution[index] = 1;// select the element
            currSum += A[index];
            find(A, currSum, index + 1, sum, solution);
            currSum -= A[index];
            solution[index] = 0;// do not select the element
            find(A, currSum, index + 1, sum, solution);
        }
        return;
    }


}


