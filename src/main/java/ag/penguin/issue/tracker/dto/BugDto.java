package ag.penguin.issue.tracker.dto;

import ag.penguin.issue.tracker.enums.Priority;
import ag.penguin.issue.tracker.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BugDto {
    private String id;
    @NotBlank(message = "Bug title cannot be null")
    private String title;
    @NotBlank(message = "Bug title cannot be null")
    private String description;
    private LocalDateTime creationTime;
    private String  developerId;
    @NotNull(message = "Bug status cannot be null")
    private Status status;
    private Priority priority;
}
