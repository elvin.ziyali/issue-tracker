package ag.penguin.issue.tracker.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AssignmentDto {
    private int index;
    private int assignedPoint;
    private List<StoryDto> assignedTasks;

}
