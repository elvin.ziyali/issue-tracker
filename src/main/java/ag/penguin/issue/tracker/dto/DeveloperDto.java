package ag.penguin.issue.tracker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeveloperDto {
    private String id;

    @NotBlank(message = "Developer name cannot be blank")
    private String name;
}
