package ag.penguin.issue.tracker.dto;

import ag.penguin.issue.tracker.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StoryDto {
    private String id;
    @NotBlank(message = "Story title cannot be empty")
    private String title;
    @NotBlank(message = "Story description cannot be empty")
    private String description;
    private LocalDateTime creationTime;
    private String developerId;
    @NotNull(message = "Story status cannot be empty")
    private Status status;
    @NotNull(message = "Story point cannot be empty")
    private Integer point;
}
