package ag.penguin.issue.tracker.repository;

import ag.penguin.issue.tracker.entity.Story;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoryRepository extends JpaRepository<Story, String> {
    List<Story> findAllByOrderByPointAsc();
}
