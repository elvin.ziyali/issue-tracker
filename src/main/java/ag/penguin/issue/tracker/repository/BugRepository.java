package ag.penguin.issue.tracker.repository;

import ag.penguin.issue.tracker.entity.Bug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BugRepository extends JpaRepository<Bug, String> {

}
