package ag.penguin.issue.tracker.repository;

import ag.penguin.issue.tracker.entity.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeveloperRepository extends JpaRepository<Developer, String> {

}
