package ag.penguin.issue.tracker.mapper;

import ag.penguin.issue.tracker.dto.DeveloperDto;
import ag.penguin.issue.tracker.entity.Developer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeveloperMapper {

    DeveloperDto mapToDto(Developer developer);

    List<DeveloperDto> mapToDtoList(List<Developer> developers);

    @Mapping(target = "id", ignore = true)
    Developer mapToEntity(DeveloperDto developerDto);

    List<Developer> mapToEntityList(List<DeveloperDto> developerDtos);
}
