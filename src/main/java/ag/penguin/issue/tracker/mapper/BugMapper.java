package ag.penguin.issue.tracker.mapper;

import ag.penguin.issue.tracker.dto.BugDto;
import ag.penguin.issue.tracker.entity.Bug;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BugMapper {

    @Mapping(target = "developerId", source = "developer.id")
    @Mapping(target = "creationTime", source = "createdAt")
    BugDto mapToDto(Bug bug);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "developer", ignore = true)
    Bug mapToEntity(BugDto bugDto);

    List<BugDto> mapToDtoList(List<Bug> bugs);

    List<Bug> mapToEntityList(List<BugDto> bugDtos);


}
