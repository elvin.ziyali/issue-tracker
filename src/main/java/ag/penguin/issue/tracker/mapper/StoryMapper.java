package ag.penguin.issue.tracker.mapper;

import ag.penguin.issue.tracker.dto.StoryDto;
import ag.penguin.issue.tracker.entity.Story;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface StoryMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "title")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "developerId", source = "developer.id")
    @Mapping(target = "creationTime", source = "createdAt")
    @Mapping(target = "point", source = "point")
    @Mapping(target = "status", source = "status")
    StoryDto mapToDto(Story story);

    @Mapping(target = "title", source = "title")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "point", source = "point")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "id", ignore = true)
    Story mapToEntity(StoryDto storyDto);

    List<Story> mapToEntityList(List<StoryDto> storyDtos);

    List<StoryDto> mapToDtoList(List<Story> stories);
}
